pipeline {
    agent any
    tools{
        nodejs "NodeJS"
    }
    environment{
        projectName = "testingscan"
        folderPath = 'reactjs-17-templates-jenkins'

    }
    stages {
        stage('Git clone') {
            steps {
                sh """ 
                git clone https://gitlab.com/devops-trainings3/reactjs-17-templates-jenkins
                
            """
                
            }
        }

       stage("Install Dependencies and Build "){
            steps{
                sh """
                node -v
                npm install
                npm run build
                """
            }
        }
          
        stage("Sonarqube Analysis "){
            steps{
                script {
                    // Configure the SonarQube server details
                    def scannerHome = tool 'sonarqube'; 
                     withSonarQubeEnv('sonarqube') {
                        sh "${tool("sonarqube")}/bin/sonar-scanner -Dsonar.projectKey=${projectName} -Dsonar.projectName=${projectName}" 
                      
           
                    }
                }
            }
        }
              stage("Check Quality Gate"){
            steps{
                script{
                       
                      
                def qg = waitForQualityGate()
                if (qg.status != 'OK') {
                    // error "message"
                    // sendTelegramMessage ! 
                    sh """
                    curl -s -X POST https://api.telegram.org/bot7037895712:AAE5ETN0gN_0JzrGxAjIn86POR_Y_mxAYxQ/sendMessage -d chat_id=681391503 -d text="Code is not passed the quality gate "
                    """
                    currentBuild.result='SUCCESS'
                    sh """
                    echo  "Pipeline aborted due to quality gate failure: ${qg.status}"
                    """
                }else{
                    currentBuild.result='SUCCESS'
                }

                }
            }
        }
        stage("Build Docker"){
             when {
                    expression{
                        currentBuild.result == 'SUCCESS'
                    }
                }
            steps{
               dir("reactjs-17-templates-jenkins"){
                 sh """
                 echo "Building Docker Images ... "
                docker build -t reactjstesting17 .
                docker tag reactjstesting17:latestsoksereihing/reactjstesting17:latest
                 """
            }
            }
        }
        stage("Push image to dockerhub repo"){
            steps{
                withCredentials([usernamePassword(credentialsId: 'dockerhub_token', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
                sh '''
                docker login -u $USERNAME -p $PASSWORD
                docker pushsoksereihing/reactjstesting17:latest
                '''
                }

            }
        }
        stage("Deploy docker container"){
            steps{
               
                 sh """
                 echo "deploying container ... "
                  docker run -d -p 5000:80 reactjstesting17
                 """
            }
        }
        stage("Send notification to Telegram"){
            steps{
                sh """
                curl -s -X POST https://api.telegram.org/bot7029620646:AAGB1dlzPGoWc-jWfQfh90qLQH5a1SuX9s4Y/sendMessage -d chat_id=6811391503 -d text="Docker Build completed successfully, please access to web via http://10.1.65.189:5000"
                """
            }
        }

        
    }
}